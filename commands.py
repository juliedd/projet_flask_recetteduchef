import click
from .app import app, db, login_manager


@app.cli.command()
@click.argument('filename')

def loaddb(filename):
    '''Creation des tables et instanciation avec les données'''
    # creation de toutes les tables
    db.create_all()

@app.cli.command()

def syncdb():
    '''Creates all missing tables.'''
    db.create_all()

@app.cli.command()
@click.argument('id')
@click.argument('pseudo')
@click.argument('password')
def newuser(id,pseudo, password):
    '''Adds a new user.'''
    from .models import Utilisateur
    from hashlib import sha256
    m = sha256()
    m.update(password.data.encode())
    password = m.hexdigest()
    u = Utilisateur(id=id,pseudo=pseudo, password=password)
    db.session.add(u)
    db.session.commit()


@app.cli.command()
@click.argument('idRecette')
@click.argument('label')
@click.argument('image_')
@click.argument('note')
@click.argument('date_')
@click.argument('pseudo')
def newrecette(idRecette,label,image_,note,date_,pseudo):
    '''Add a new recipe'''
    from .models import Recette
    r = Recette(idRecette=idRecette, label=label, image_=image_, note=note, date_=date_, pseudo=pseudo)
    db.session.add(r)
    db.session.commit()
