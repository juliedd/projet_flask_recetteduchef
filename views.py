from .app import app, db
import math as m
from flask import render_template, url_for, redirect, request, session, abort, current_app
from flask_login import login_user, current_user, logout_user, login_required
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField,FileField, PasswordField,SubmitField, TextAreaField, IntegerField, validators,RadioField, FieldList, FormField
from wtforms.validators import DataRequired, Email
from hashlib import sha256
from wtforms.widgets import Input
from .flask_pager import Pager

import yaml, os.path

class NumberInput(Input):
    """
    Creates `<input type=number>` widget
    """
    input_type="number"

class LoginForm(FlaskForm):
    """
    Permet d'identifier une personne
    """
    pseudo = StringField('Pseudo',validators=[DataRequired()])
    password = PasswordField('Mot de passe',validators=[DataRequired()])

    def get_authenticated_user(self):
        user = Utilisateur.query.get(self.pseudo.data)
        if user is None :
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

class RechercheForm(FlaskForm):
    """
    Permet de faire des recherches au niveau des recettes
    """
    texte = StringField('Rechercher')

class SupprForm(FlaskForm):
    supp = HiddenField()

class InsForm(FlaskForm):
    """
    Permet à une personne de s'inscrire
    """
    pseudo = StringField('Pseudo',validators=[DataRequired()])
    password = PasswordField('Mot de passe',validators=[DataRequired()])

class IngreForm(FlaskForm):
    """
    Permet d'ajouter les élements d'un ingredient
    """
    nom= StringField(validators=[DataRequired()])
    quantite= IntegerField(widget =NumberInput(),validators=[DataRequired(), validators.NumberRange(0,(5000),None)])
    unite=StringField(validators=[DataRequired()])
    idIngredientOriginal = HiddenField()

class EtapeForm(FlaskForm):
    """
    Permet d'ajouter les élements d'une étape
    """
    id_ = IntegerField(widget=NumberInput(), validators=[DataRequired()])
    etape_t= TextAreaField(render_kw={'class': 'form-control', 'rows': 2, 'cols':100 }, validators=[DataRequired()])

class ModifForm(FlaskForm):
    """
    Permet de modfier les éléments d'uns recette avec ses ingrédients et ses étapes
    """
    nom_recette = StringField('Nom de la recette : ', validators=[DataRequired()])
    ingredients= FieldList(FormField(IngreForm))
    etapes = FieldList(FormField(EtapeForm))
    sub = SubmitField(label='Valider les modifications ')

class RecetteForm(FlaskForm):
    """
    Permet d'ajouter une recette
    """
    nomR = StringField('Nom de la recette : ', validators=[DataRequired()])
    image_ = TextAreaField(render_kw={'class': 'form-control', 'rows': 2, 'cols':100 }, validators=[DataRequired()])
    note =  IntegerField('Note : ',widget =NumberInput(),validators=[DataRequired(), validators.NumberRange(0,5,None)])
    ingredients = FieldList(FormField(IngreForm))
    etapes = FieldList(FormField(EtapeForm))

class ChoixNbIngrEtapes(FlaskForm):
    """
    Permet de récupérer le nombre d'ingrédients et d'étapes à ajouter
    """
    nbIngr= IntegerField("Nombre d'ingrédients : ",widget =NumberInput(), validators=[DataRequired(), validators.NumberRange(0,20,None)])
    nbEtapes= IntegerField("Nombre d'étapes : ",widget =NumberInput(), validators=[DataRequired(), validators.NumberRange(0,20,None)])
    sub = SubmitField(label='Valider les modifications ')

class TriRecetteForm(FlaskForm):
    """
    Permet de choisir le tri des recettes
    """
    choix= RadioField('Trier la recherche', choices=[('1','Ordre Alphabétique'),('2','Date du plus récent au plus ancien'),('3','Date du plus ancien au plus récent'),('4','Note plus élevée à la plus faible')])

class ComForm(FlaskForm):
    """
    Permet d'ajouter un commentaire
    """
    texte = TextAreaField('Commentaire',validators=[DataRequired()])
    note = IntegerField('Note',widget =NumberInput(),validators=[DataRequired(), validators.NumberRange(0,5,None)])

class ModifProfilForm(FlaskForm):
    """
    Permet de modifier un profil
    """
    avatar= TextAreaField('Mon avatar : ', render_kw={'class': 'form-control', 'rows': 2, 'cols':100 },validators=[DataRequired()])
    pseudo= StringField('Mon pseudo :',validators=[DataRequired()])
    mail= StringField('Mon mail : ', validators=[DataRequired(),Email()])

class ModifMDPForm(FlaskForm):
    """
    Permet de modifier un mot de passe
    """
    mdp = PasswordField('Mot de passe : ',validators=[DataRequired()])
    mdpConfirmation = PasswordField('Mot de passe de confirmation :',validators=[DataRequired()])
# Fonctionnalités accésible aux personnes non connectées

@app.route("/login/", methods=("GET","POST",))
def login():
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
        if user :
            login_user(user)
            return redirect(url_for('accueil_connecte'))
    return render_template("login.html",form=f,title="Login")


@app.route('/sinscrire', methods=['GET', 'POST'])
def sinscrire():
    fi = InsForm()
    probleme = "Après avoir tout rempli vous pourrez accéder à toutes les focntionnalitées du site"
    if fi.validate_on_submit():
        from .models import Utilisateur
        from hashlib import sha256

        pseudo = request.form['pseudo']
        password = request.form['password']

        if not pseudo_utilise(pseudo) :
            ajouter_utilisateur_bd(pseudo,password)
            return redirect(url_for("login"))
        else :
            probleme = "Ce nom d'utilisateur est déjà utilisé"

    return render_template(
    "sinscrire.html",
    title="S'inscrire",
    form = InsForm(),
    probleme =probleme
    )


@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("accueil"))


@app.route("/accueil", methods=['GET', 'POST'])
def accueil():
    all_infos =get_recettes()
    if len(all_infos) != 0 :
        count = round(len(all_infos))
    else :
        count = 1
    page = int(request.args.get('page', 1))
    data = range(count)
    pager = Pager(page, count)
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    recette = get_recettes()
    data_to_show = recette[skip: skip + limit]
    rech = RechercheForm()
    if rech.validate_on_submit():
        r=rech.texte.data
        if r is '':
            r='_'
        return redirect(url_for("recherche",rec=r))

    return render_template(
    "accueil.html",
    title="Accueil",
    rech = rech,
    recettes = data_to_show,
    pages=pages
    )

@app.route("/consulterRecette/<int:idRecette>")
def accueil_non_connecte_consulterRecette(idRecette):

    rech = RechercheForm()
    if rech.validate_on_submit():
        return redirect(url_for("recherche",rec=rech.texte.data))

    if infos_commentaires(idRecette) != [] :
        note = infos_commentaires(idRecette)[0][0].noteCom
    else :
        note = 0
    return render_template(
    "consulterRecetteNonConnecte.html",
    rech=rech,
    title = get_recette(idRecette).label,
    createur = get_recette(idRecette).pseudo,
    image = get_recette(idRecette).image_,
    noteG = get_recette(idRecette).note,
    info_ingredients=infos_ingredientsQuantite(get_ingredientRecette(idRecette)),
    commentaires = infos_commentaires(idRecette),
    etapes = getEtapes(idRecette),
    note = note
    )


@app.route("/trier/<int:tri>/<string:lettre>")
def trier(tri,lettre):
    if current_user.is_authenticated :
        lien = current_user.avatar
        recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo))
        listeInfo=[]
        infos = info_fav_utilisateur(current_user.pseudo)
        for elem in infos:
            listeInfo.append(elem.idRecette)
        favoris = get_all_nom_fav(listeInfo)
    else:
        recette_ajoutees=[]
        lien=""
        favoris=[]
    r=get_recette_lettre(lettre)
    if tri == 1:
        recettes = sorted(r, key=lambda recette: recette.label)
    elif tri == 2:
        recettes = sorted(r, key=lambda recette: recette.date_, reverse=True)
    elif tri == 3 :
        recettes = sorted(r, key=lambda recette: recette.date_)
    elif tri == 4 :
        recettes = sorted(r, key=lambda recette: recette.note, reverse=True)

    all_infos = recettes
    if len(all_infos) != 0 :
        count = round(len(all_infos))
    else :
        count = 1
    page = int(request.args.get('page', 1))
    data = range(count)
    pager = Pager(page, count)
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = recettes[skip: skip + limit]
    return render_template(
    "trier.html",
    title="Trier",
    recettes = data_to_show,
    recette_ajoutees=recette_ajoutees,
    favoris=limiterListe(favoris),
    pages=pages,
    lien = lien
    )

@app.route("/rechercher/<string:rec>",methods=['GET', 'POST'])
def recherche(rec):
    if current_user.is_authenticated :
        lien = current_user.avatar
        recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo))
        listeInfo=[]
        infos = info_fav_utilisateur(current_user.pseudo)
        for elem in infos:
            listeInfo.append(elem.idRecette)
        favoris = get_all_nom_fav(listeInfo)
    else:
        lien=""
        favoris=[]
        recette_ajoutees=[]
    tri =TriRecetteForm()
    if tri.validate_on_submit():
        leTri = int(tri.choix.data)
        return redirect(url_for("trier",tri=leTri,lettre=rec))

    r=rec
    c = get_recette_lettre(rec)
    if len(c) != 0 :
        count = round(len(c))
    else :
        count = 1
    page = int(request.args.get('page', 1))
    data = range(count)
    pager = Pager(page, count)
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = c[skip: skip + limit]
    return render_template(
        "rechercher.html",
        r=r,
        radioTri= tri,
        title="Recherche",
        recette_ajoutees=recette_ajoutees,
        favoris=limiterListe(favoris),
        recettes = data_to_show,
        pages=pages,
        lien = lien
        )

# Fonctionnalités accésible aux personnes connectées

@app.route("/accueil_connecte",methods=['GET', 'POST'])
@login_required
def accueil_connecte():
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)

    all_infos = get_recettes()
    if len(all_infos) != 0 :
        count = round(len(all_infos))
    else :
        count = 1
    page = int(request.args.get('page', 1))
    data = range(count)
    pager = Pager(page, count)
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    recette = all_infos
    data_to_show = recette[skip: skip + limit]

    rech = RechercheForm()
    if rech.validate_on_submit():
        return redirect(url_for("recherche",rec=rech.texte.data))

    return render_template(
    "accueil_connecte.html",
    title="Accueil",
    recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
    rech=rech,
    favoris= limiterListe(favoris),
    recettes = data_to_show,
    pages = pages,
    lien = current_user.avatar
    )

@app.route("/voirMonProfil",methods=['GET', 'POST'])
@login_required
def voirProfil():
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)
    infos_perso = get_utilisateurs(current_user.pseudo)
    return render_template(
        "voirProfil.html",
        title="Voir mon profil",
        recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
        favoris= limiterListe(favoris),
        afficher = infos_perso,
        lien = current_user.avatar
    )

@app.route("/modifierMonMotDePasse/<int:enregistrer>",methods=['GET', 'POST'])
@login_required
def modifierMDP(enregistrer):
    f = ModifMDPForm()
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)
    infos_perso = get_utilisateurs(current_user.pseudo)

    if enregistrer==1 :
        mdp = request.form['mdp']
        mdpConfirmation = request.form['mdpConfirmation']
        if verification_Modif_MDP(mdp,mdpConfirmation) :
            modifier_MDP(mdp,current_user.pseudo)
            return redirect(url_for('voirProfil'))
        return redirect(url_for('modifierMDP',enregistrer=2))

    return render_template(
        "modifierMDP.html",
        title="Modifier mon mot de passe",
        favoris= limiterListe(favoris),
        recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
        enregistrer=enregistrer,
        form= f,
        afficher = infos_perso,
        lien = current_user.avatar
    )

@app.route("/modifierMonProfil/<int:enregistrer>",methods=['GET', 'POST'])
@login_required
def modifierProfil(enregistrer):
    f = ModifProfilForm(avatar=current_user.avatar, pseudo=current_user.pseudo, mail = current_user.mail)

    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)
    infos_perso = get_utilisateurs(current_user.pseudo)

    if enregistrer==1 :
        a = request.form['avatar']
        p = request.form['pseudo']
        m = request.form['mail']
        if verification_Modif_Profil(current_user.pseudo,p):
            modifier_Profil(current_user.pseudo,p,a,m)
            return redirect(url_for('voirProfil'))
        return redirect(url_for('modifierProfil',enregistrer=2))

    return render_template(
        "modifierProfil.html",
        title="Modifier mon profil",
        favoris= limiterListe(favoris),
        enregistrer=enregistrer,
        form= f,
        recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
        afficher = infos_perso,
        lien = current_user.avatar
    )

@app.route("/accueil_connecte/RecetteAFaire/<int:fav>")
@login_required
def accueil_connecte_recettes_fav_ajout(fav):
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    all_infos=[]
    for elem in infos:
        listeInfo.append(elem.idRecette)
        if get_recette(elem.idRecette) not in all_infos :
            all_infos.append(get_recette(elem.idRecette))
    favoris = get_all_nom_fav(listeInfo)

    if len(all_infos) != 0 :
        count = round(len(all_infos))
    else :
        count = 1
    page = int(request.args.get('page', 1))
    data = range(count)
    pager = Pager(page, count)
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']

    if fav ==1:
        recette = all_infos
    else :
        recette = get_recettes_pseudo(current_user.pseudo)
    data_to_show = recette[skip: skip + limit]

    return render_template(
    "accueil_connecte_recette_a_faire.html",
    title="Recette à faire",
    favoris= limiterListe(favoris),
    recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
    recettes = data_to_show,
    pages = pages,
    fav=fav,
    lien = current_user.avatar
    )

@app.route("/consulterRecetteConnecte/<int:idRecette>/<int:favoris>", methods=['GET', 'POST'])
@login_required
def accueil_connecte_consulterRecette(idRecette,favoris):

    if favoris ==1 and favoris_existe(current_user.pseudo,idRecette) :
        supprimer_fav_recette(current_user.pseudo,idRecette)
    else :
        if favoris == 1 :
            ajouter_favoris(idRecette,current_user.pseudo)

    if favoris_existe(current_user.pseudo,idRecette):
        coeur="https://images.emojiterra.com/google/android-10/512px/2764.png"
    else :
        coeur="https://cdn.icon-icons.com/icons2/494/PNG/512/heart_icon-icons.com_48290.png"

    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)

    s = SupprForm()
    if s.validate_on_submit():
        supprimer_all_recette(idRecette,current_user.pseudo)
        return redirect(url_for("accueil_connecte"))

    rech = RechercheForm()
    if rech.validate_on_submit():
        return redirect(url_for("recherche",rec=rech.texte.data))

    elif infos_commentaires(idRecette) != [] :
        note = infos_commentaires(idRecette)[0][0].noteCom
    else :
        note = 0
    recette_ajoutees=get_recettes_pseudo(current_user.pseudo)
    return render_template(
    "consulterRecetteConnecte.html",
    imageFavoris = coeur,
    rech=rech,
    recette_ajoutees= limiterListe(recette_ajoutees),
    createur = get_recette(idRecette).pseudo,
    idR = idRecette,
    title = get_recette(idRecette).label,
    image = get_recette(idRecette).image_,
    favoris=limiterListe(favoris),
    noteG = get_recette(idRecette).note,
    info_ingredients=infos_ingredientsQuantite(get_ingredientRecette(idRecette)),
    commentaires = infos_commentaires(idRecette),
    etapes = getEtapes(idRecette),
    note = note,
    form =s,
    lien = current_user.avatar
    )


@app.route("/modifierRecette/<int:idRecette>/<int:repere>", methods=['GET', 'POST'])
@login_required
def modifierRecette(idRecette,repere):

    f=ModifForm()

    if repere==1:
        nomR = (idRecette,request.form['nom_recette'])
        resultsIngredients = []
        resultsEtapes =[]
        idIngredientsOriginaux=[]
        for idx, data in enumerate(f.etapes.data):
            resultsEtapes.append((data['id_'],data['etape_t']))
        for idx, data in enumerate(f.ingredients.data):
            resultsIngredients.append((data['nom'],data['quantite'],data['unite']))
            idIngredientsOriginaux.append(data['idIngredientOriginal'])
        verification_Modif(nomR,resultsIngredients,resultsEtapes,idIngredientsOriginaux)
        return redirect(url_for("accueil_connecte_consulterRecette",idRecette=idRecette,favoris=0))

    else :
        f=ModifForm(nom_recette=get_recette(idRecette).label)
        tous_ingredients_recette = get_ingredientRecette(idRecette)
        ingredients_all = []
        for elem in tous_ingredients_recette:
            infos=(get_infoIngredients(elem.idIngredient).nomI, elem.quantite, elem.unite,elem.idIngredient)
            ingredients_all.append(infos)
        tous_etapes_recette = getEtapes(idRecette)

        for nomI, quantite, unite, idI in ingredients_all:
            ingreform = IngreForm()
            ingreform.nom = nomI
            ingreform.quantite = int(quantite)
            ingreform.unite = unite
            ingreform.idIngredientOriginal = int(idI)
            f.ingredients.append_entry(ingreform)

        for elem in tous_etapes_recette:
            etapeform = EtapeForm()
            etapeform.id_ = int(elem.idEtape)
            etapeform.etape_t = elem.texte
            f.etapes.append_entry(etapeform)

    return render_template(
        "modifierRecette.html",
        title = "Modifier Recette",
        repere=repere,
        recette_ajoutees=get_recettes_pseudo(current_user.pseudo),
        form=f,
        idRecette=idRecette,
        lien = current_user.avatar
    )


@app.route("/consulterRecetteConnecte/<int:idRecette>/ajoutCommentaire", methods=['GET', 'POST'])
@login_required
def ajouterCommentaireRecette(idRecette):
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)

    fi = ComForm()
    probleme = "Vous devez tous remplir pour que votre commentaire soit publié ! La note doit être comprise entre 1 et 5 !"
    if fi.validate_on_submit():
        texte = request.form['texte']
        note = request.form['note']
        pseudo = current_user.pseudo
        ajouterCommentaire(idRecette,texte,note,pseudo)
        return redirect(url_for("accueil_connecte_consulterRecette",idRecette=idRecette,favoris=0))

    return render_template(
    "ajoutCommentaire.html",
    title ="Ajouter Commentaire",
    form = fi,
    id = idRecette,
    favoris=limiterListe(favoris),
    recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
    probleme = probleme,
    lien = current_user.avatar
    )

@app.route("/choixAjouterRecette", methods=['POST','GET'])
@login_required
def choixAjouterRecette():
    choix = ChoixNbIngrEtapes()

    if choix.validate_on_submit():
        i = request.form['nbIngr']
        e = request.form['nbEtapes']
        return redirect(url_for('ajouterRecette',ajout=0,ingr=i,etapes=e))

    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)
    return render_template(
    "choixAjouterRecette.html",
    title = "Ajouter Recette",
    favoris=limiterListe(favoris),
    recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
    form = choix,
    lien = current_user.avatar
    )


@app.route("/ajouterRecette/<int:ajout>/<int:ingr>/<int:etapes>", methods=['POST','GET'])
@login_required
def ajouterRecette(ajout,ingr,etapes):
    listeInfo=[]
    infos = info_fav_utilisateur(current_user.pseudo)
    for elem in infos:
        listeInfo.append(elem.idRecette)
    favoris = get_all_nom_fav(listeInfo)

    f = RecetteForm()
    if ajout==1:
        resultsIngredients = []
        resultsEtapes =[]
        for idx, data in enumerate(f.etapes.data):
            resultsEtapes.append((data['id_'],data['etape_t']))
        for idx, data in enumerate(f.ingredients.data):
            resultsIngredients.append((data['nom'],data['quantite'],data['unite']))
        nomR = request.form['nomR']
        noteR = request.form['note']
        imageR = request.form['image_']
        pseudo = current_user.pseudo
        ajoutRecette(nomR,noteR,imageR,pseudo,resultsIngredients,resultsEtapes)
        return redirect(url_for('accueil_connecte'))

    for i in range (ingr):
        ingreform = IngreForm()
        ingreform.nom = ""
        ingreform.unite = ""
        f.ingredients.append_entry(ingreform)

    for ii in range (etapes) :
        etapeform = EtapeForm()
        etapeform.etape_t = ""
        f.etapes.append_entry(etapeform)


    return render_template(
    "ajouterRecette.html",
    title="Ajouter Recette",
    favoris=limiterListe(favoris),
    ingr=ingr,
    recette_ajoutees=limiterListe(get_recettes_pseudo(current_user.pseudo)),
    etapes=etapes,
    form=f,
    lien = current_user.avatar
    )
