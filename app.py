from flask import Flask, render_template, request, current_app
from flask_bootstrap import Bootstrap
import os.path
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os



def mkpath(p):
    return os.path.normpath(
    os.path.join(os.path.dirname(__file__), p))

app = Flask(__name__)

app.config['PAGE_SIZE'] = 2
app.config['VISIBLE_PAGE_COUNT'] = 5

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../projet.db'))

app.config['SECRET_KEY'] = "5935af39-aca9-45fc-a1c9-8dbf16573d71"

login_manager = LoginManager(app)

login_manager.login_view = "login"

db = SQLAlchemy(app)
DEBUG = True