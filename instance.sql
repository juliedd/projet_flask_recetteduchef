
INSERT INTO ingredient(idIngredient, nomI) VALUES (1,"Sucre blanc");
INSERT INTO ingredient(idIngredient, nomI) VALUES (2,"Farine");
INSERT INTO ingredient(idIngredient, nomI) VALUES (3,"Oeuf(s)");
INSERT INTO ingredient(idIngredient, nomI) VALUES (4,"Lait");
INSERT INTO ingredient(idIngredient, nomI) VALUES (5,"Vanille");
INSERT INTO ingredient(idIngredient, nomI) VALUES (6,"Rhum");
INSERT INTO ingredient(idIngredient, nomI) VALUES (7,"Levure chimique");
INSERT INTO ingredient(idIngredient, nomI) VALUES (8,"Levure boulangère");
INSERT INTO ingredient(idIngredient, nomI) VALUES (9,"Beurre");
INSERT INTO ingredient(idIngredient, nomI) VALUES (10,"Sel");
INSERT INTO ingredient(idIngredient, nomI) VALUES (11,"Chocolat");

INSERT INTO ComposerI(quantite,idIngredient,idRecette, unite) VALUES (220,2,1,"gr");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (2,3,1,"unité");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (20,1,1,"gr");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (37,9,1,"gr");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES(44,4,1,"cl");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (5,6,1,"cl");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (50,1,2,"gr");
INSERT INTO ComposerI(quantite, idIngredient,idRecette, unite) VALUES (150,2,3,"gr");

INSERT INTO Etape(idEtape,idRecette,texte) VALUES (1,1,"Tamiser la farine");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (2,1,"Mélanger la vanille avec le sucre et la farine");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (3,1,"Faire fondre le beurre et ajouter le à la farine avec les oeufs");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (4,1,"Ajouter le lait dans la préparation et mélanger jusqu'à obtention d'une pâte lisse");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (5,1,"Ajouter le rhum");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (6,1,"La pâte à crêpe est prête !");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (7,2,"La pâte à gaufre est prête !");
INSERT INTO Etape(idEtape,idRecette,texte) VALUES (8,3,"La pâte à gougère est prête !");


INSERT INTO Recette(idRecette,label,image_,note,date_,pseudo) VALUES(1,"Crêpes","https://static3.hervecuisine.com/wp-content/uploads/2010/11/recette-crepes-chandeleur-2018.jpg",4,'2019-12-06 15:23:12',"Laure du 45");
INSERT INTO Recette(idRecette,label,image_,note,date_,pseudo) VALUES(2,"Gaufres","https://www.marciatack.fr/wp-content/uploads/2014/09/gaufre-rapide-2.jpg",3,'2019-12-09 10:00:00',"Juju du 45");
INSERT INTO Recette(idRecette,label,image_,note,date_,pseudo) VALUES(3,"Gougères","https://static.lexpress.fr/medias_9213/w_1530%2Ch_665%2Cc_crop%2Cx_0%2Cy_785/w_640%2Ch_360%2Cc_fill%2Cg_north/v1391092316/gougeres-au-comte_4717253.jpg",3,'2019-11-09 10:00:00',"Juju du 45");

INSERT INTO Utilisateur(id,avatar,mail,pseudo,password) VALUES (1,"https://image.flaticon.com/icons/png/512/94/94386.png","juju@gmail.com","Juju du 45","aze");
INSERT INTO Utilisateur(id,avatar,mail,pseudo,password) VALUES (2,"https://image.flaticon.com/icons/png/512/94/94386.png","louloutte@gmail.com","Laure du 45","az");

INSERT INTO Commentaire(idCom,texteCom,noteCom,dateCom,idRecette,pseudo)
VALUES(1,"La recette est très bien pour faire des crêpes légères !",4,'2019-12-06 10:00:00',1,"Laure du 45");
