from flask_login import UserMixin
from .app import db, login_manager
import math as m
import datetime
from hashlib import sha256

class Utilisateur(db.Model, UserMixin):
    __tablename__='utilisateur'
    id = db.Column(db.Integer)
    avatar = db.Column(db.String(2000))
    mail = db.Column(db.String(60))
    pseudo = db.Column(db.String(25), primary_key=True)
    password = db.Column(db.String(30))

    def get_id(self):
        return self.pseudo
        
    def __repr__(self):
        return "<Utilisateur (%d) %s %s>" %(self.id, self.pseudo,self.password)


class Recette(db.Model):
    __tablename__='recette'
    idRecette = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(100))
    image_ = db.Column(db.String(2000))
    note = db.Column(db.Integer)
    date_ = db.Column(db.String)
    pseudo = db.Column(db.String(25), db.ForeignKey("utilisateur.pseudo"))

    def __repr__(self):
        return "<Recette (%d) %s>" %(self.idRecette, self.label)

class Ingredient(db.Model):
    __tablename__='ingredient'
    idIngredient = db.Column(db.Integer, primary_key=True)
    nomI = db.Column(db.String(25))
    def __repr__(self):
        return "<Ingredient idIngredient %d, nomI %s >" %(self.idIngredient, self.nomI)

class Etape(db.Model):
    __tablename__='etape'
    idEtape = db.Column(db.Integer, primary_key=True)
    idRecette = db.Column(db.Integer, db.ForeignKey("recette.idRecette"))
    texte = db.Column(db.String(500))
    def __repr__(self):
        return "<Etape idEtape %d, texte %s, idRecette %d >" %(self.idEtape, self.texte, self.idRecette)

class Commentaire(db.Model):
    __tablename__='commentaire'
    idCom = db.Column(db.Integer, primary_key=True)
    texteCom = db.Column(db.String(500))
    noteCom = db.Column(db.Integer)
    dateCom = db.Column(db.DateTime)
    idRecette = db.Column(db.Integer, db.ForeignKey("recette.idRecette"))
    pseudo = db.Column(db.String(25), db.ForeignKey("utilisateur.pseudo"))

    def __repr__(self):
        return "<Commentaire idCom %d, texteCom %s, noteCom %d, dateCom %s ,idRecette %d, id %s>" %(self.idCom, self.texteCom, self.noteCom, self.dateCom, self.idRecette, self.pseudo)

class ComposerI(db.Model):
    __tablename__='composerI'
    quantite = db.Column(db.Integer)
    idIngredient = db.Column(db.Integer,db.ForeignKey("ingredient.idIngredient"), primary_key=True)
    idRecette = db.Column(db.Integer,db.ForeignKey("recette.idRecette"), primary_key=True)
    unite = db.Column(db.String(25))

    def __repr__(self):
        return "ComposerI (ingredient %d, recette %d) quantité %d unité %s" %(self.idIngredient, self.idRecette, self.quantite,self.unite)

class Favoris(db.Model):
    __tablename__='favoris'
    pseudo = db.Column(db.String(25), db.ForeignKey("utilisateur.pseudo"), primary_key=True)
    idRecette = db.Column(db.Integer,db.ForeignKey("recette.idRecette"), primary_key=True)

    def __repr__(self):
        return "Favoris pseudo %d idRectte %d" %(self.pseudo, self.idRecette)

#all
def getIdMax(listeId):
    return max(listeId)

def verification_Modif(Idnom,listeIngre,listeEtapes,idIngredientsOriginaux):
    """
    permet de vérifier s'il y a des modifications à enregistrer dans la base de données pour chaque élément
    pour vérifier tout cela, on va comparer les informations originales de la recette avec les informations qui viennent d'être entrer par l'utilisateur
    """
    verif_modif_NomR(Idnom)
    verif_modif_Ingredient(listeIngre,Idnom[0],idIngredientsOriginaux)
    verif_modif_etapes(listeEtapes,Idnom[0])

def limiterListe(liste):
    if len(liste)>3:
        return liste[0:3]
    return liste

#recette
def verif_modif_NomR(nomId):
    rOriginal = get_recette(nomId[0])
    nomChange = nomId[1]
    if not nomR_existe(nomChange) :
        rOriginal.label = nomChange
        db.session.commit()

def nomR_existe(nom):
    recettes_ = get_recettes()
    recettes=[]
    for elem in recettes_ :
        recettes.append(elem.label)
    return nom in recettes

def get_recettes():
    return Recette.query.all()

def get_recette(id):
    return Recette.query.get(id)

def get_recettes_pseudo(pseudo):
    return Recette.query.filter(Recette.pseudo==pseudo).all()

def get_recette_lettre(rec):
    if rec=='_':
        return get_recettes()
    rec=rec+'%'
    cc=db.engine.execute("SELECT * from Recette where like(?,Recette.label)=1", (rec))
    c=[]
    for row in cc:
        c.append(row)
    return c

def moyenneGeneralRecette(idRe):
    infos_com = infos_commentaires(idRe)
    nbCo = len(infos_com)+1
    total = get_recette(idRe).note
    for info in infos_com:
       total += info[0].noteCom
    return m.floor(total/nbCo)

def supprimer_all_recette(idRecette,pseudo):
    supprimer_all_commentaires_Recette(idRecette)
    supprimer_all_etapes_Recette(idRecette)
    supprimer_all_ingredient_Recette(idRecette)
    supprimer_fav_recette(pseudo,idRecette)
    recette = get_recette(idRecette)
    db.session.delete(recette)
    db.session.commit()

def ajoutRecette(nomR,noteR,imageR,pseudo,listeIngre,listeEtapes):
    liste_id_R = []
    liste_R= get_recettes()
    for el in liste_R:
        liste_id_R.append(el.idRecette)
    ii = getIdMax(liste_id_R)+1
    dateR = datetime.datetime.now()
    rr = Recette(idRecette = ii, label = nomR, image_ = imageR, note = noteR, date_ = dateR, pseudo =pseudo)
    db.session.add(rr)
    db.session.commit()

    for (nomI,quantite_,unite_) in listeIngre:
        ajouter_ingredient(nomI ,quantite_ ,unite_ ,ii,ingredient_existe(nomI))
    
    lEtapes = sorted(listeEtapes, key=lambda etape: etape[0])
    liste_id = []
    liste= get_all_etapes()
    for elem in liste:
        liste_id.append(elem.idEtape)

    i = getIdMax(liste_id)+1
    for (idd,texte) in listeEtapes :
        b = Etape(idRecette = ii, idEtape =i, texte=texte)
        db.session.add(b)
        db.session.commit()
        i+=1

#commentaire 

def get_all_commentaires():
    return Commentaire.query.all()

def get_commentairesRecette(idR):
    return Commentaire.query.filter(Commentaire.idRecette==idR).all()

def getInfoCommentaire(idC):
    return Commentaire.query.get(idC)

def infos_commentaires(idRecette):
    infos_commentaires = []
    all_com = get_commentairesRecette(idRecette)
    for element in all_com:
        infos_commentaire=[]
        infos_commentaire.append(getInfoCommentaire(element.idCom))
        infos_commentaire.append(element.pseudo)
        infos_commentaires.append(infos_commentaire)
    return infos_commentaires

def supprimer_all_commentaires_Recette(idRecette):
    all_commentaires = get_commentairesRecette(idRecette)
    for i in all_commentaires :
        db.session.delete(i)
        db.session.commit()

def ajouterCommentaire(idRecette,texte,note,pseudo):
    all_infoCom = get_all_commentaires()
    listeId =[]
    for elem in all_infoCom:
        listeId.append(elem.idCom)
    idCom = getIdMax(listeId)+1
    dateCom = datetime.datetime.now()
    a = Commentaire(idCom = idCom, texteCom = texte, noteCom = note, dateCom = dateCom, idRecette = idRecette, pseudo = pseudo)
    db.session.add(a)
    db.session.commit()
    recette = get_recette(idRecette)
    n =  m.floor((recette.note + int(note))/2)
    re = Recette.query.filter(Recette.idRecette==idRecette)
    re.note = n
    db.session.commit()

#Etapes
def get_all_etapes():
    return Etape.query.all()

def getEtapes(idRecette):
    return Etape.query.filter(Etape.idRecette==idRecette).all()
   
def supprimer_all_etapes_Recette(idRecette):
    all_etapes = getEtapes(idRecette)
    for i in all_etapes :
        db.session.delete(i)
        db.session.commit()

def verif_modif_etapes(listeEtapes,idR):
    all_etapes = getEtapes(idR)
    lEtapes = sorted(listeEtapes, key=lambda etape: etape[0])
    liste_id = []
    i=0
    for ee in all_etapes:
        liste_id.append(ee.idEtape)
    supprimer_all_etapes_Recette(idR)

    for (id,texte) in lEtapes:
        b = Etape(idRecette = idR, idEtape =liste_id[i], texte=texte)
        db.session.add(b)
        db.session.commit()
        i+=1

#Ingrédient dans recette

def get_all_ingredient():
    return Ingredient.query.all()

def ingredient_existe(nom):
    allI = get_all_ingredient()
    liste =[]
    for ing in allI :
        liste.append(ing.nomI)
    return nom in liste

def ingredient_recette(idR,idI):
    return ComposerI.query.filter(ComposerI.idRecette==idR,ComposerI.idIngredient==idI).all()

def quantite_existeIng(nomI,quantite):
    i = get_composerI(get_ingredientId(nomI)[0].idIngredient)
    return i[0].quantite == quantite

def get_ingredientId(nomI):
    return Ingredient.query.filter(Ingredient.nomI==nomI).all()

def get_all_composerI(idR):
    return ComposerI.query.filter(ComposerI.idRecette==idR).all()

def get_composerI(ele):
    return ComposerI.query.filter(ComposerI.idIngredient==ele).all()

def get_ingredientRecette(idR):
    return ComposerI.query.filter(ComposerI.idRecette==idR).all()

def get_infoIngreNom(nom):
    return Ingredient.query.filter(Ingredient.nomI==nom).all()

def get_infoIngredients(idI):
    return Ingredient.query.get(idI)

def infos_ingredientsQuantite(info_recettes):
    info_ingredients=[]
    for element in info_recettes :
        info_ingredient=[]
        info_ingredient.append(element.quantite)
        info_ingredient.append(element.unite)
        infos = get_infoIngredients(element.idIngredient)
        info_ingredient.append(infos)
        info_ingredients.append(info_ingredient)
    return info_ingredients

def supprimer_all_ingredient_Recette(idRecette):
    all_composerI = get_all_composerI(idRecette)
    for i in all_composerI :
        db.session.delete(i)
        db.session.commit()

def verif_modif_Ingredient(listeIngre,idR,idIngredientsOriginaux):
    ii=""
    i=0
    for (nomI,quantite_,unite_) in listeIngre:
        if not ingredient_existe(nomI) :
            r = composerIngredientRecette(idIngredientsOriginaux[i],idR)[0]
            db.session.delete(r)
            db.session.commit()
            ajouter_ingredient(nomI ,quantite_ ,unite_ ,idR,False)
        else :
            idd = get_infoIngreNom(nomI)[0].idIngredient
            if not quantite_existeIng(nomI,quantite_) :
               listeIdComposants = get_composerI(idd)
               for ele in listeIdComposants:
                    if ele.idRecette == idR :
                        ii = ele
               ii.quantite = int(quantite_)
               db.session.commit()
        i+=1
        idd = get_infoIngreNom(nomI)[0].idIngredient
        ingre = ingredient_recette(idR,idd)[0]
        if ingre.unite != unite_ :
            ingre.unite = unite_
            db.session.commit()

def composerIngredientRecette(idI,idR):
    return ComposerI.query.filter(ComposerI.idRecette==idR, ComposerI.idIngredient==idI).all()

def ajouter_ingredient(nomI,quantite,unite,idR,existe):
    if not existe :
        listeId=[]
        for elem in get_all_ingredient():
            listeId.append(elem.idIngredient)
        idd = getIdMax(listeId)+1
        a = Ingredient(idIngredient=idd,nomI=nomI)
        db.session.add(a)
        db.session.commit()
    else :
        idd = get_infoIngreNom(nomI)[0].idIngredient

    b = ComposerI(quantite = quantite, idIngredient = idd, idRecette=idR,unite=unite)
    db.session.add(b)
    db.session.commit()
#Favoris

def info_fav_utilisateur(pseudo):
    return Favoris.query.filter(Favoris.pseudo == pseudo).all()
def favoris_existe(pseudo,idRecette):
    r = Favoris.query.filter(Favoris.pseudo == pseudo, Favoris.idRecette==idRecette).all()
    return r !=[]

def fav_utilisateur(pseudo,idRecette):
    return Favoris.query.filter(Favoris.pseudo== pseudo, Favoris.idRecette==idRecette).all()

def  ajouter_favoris(idRecette,pseudo):
    if not favoris_existe(pseudo,idRecette):
        b = Favoris(idRecette=idRecette,pseudo=pseudo)
        db.session.add(b)
        db.session.commit()

def get_all_nom_fav(listeIdR):
    infosR =[]
    for idR in listeIdR:
        infosR.append((idR,get_recette(idR).label))
    return infosR

def supprimer_fav_recette(pseudo,idRecette):
    if fav_utilisateur(pseudo,idRecette) !=[]:
        r=fav_utilisateur(pseudo,idRecette)[0]
        db.session.delete(r)
        db.session.commit()

#Utilisateur

def getAllIdUser():
    return Utilisateur.query.all()

def get_all_utilisateurs():
    return Utilisateur.query.all()

def get_utilisateurs(p):
    return Utilisateur.query.get(p)

def get_utilisateurInfo(p):
    return Utilisateur.query.get(p)

def pseudo_utilise(pseudo):
    allU = get_all_utilisateurs()
    liste =[]
    for uti in allU :
        liste.append(uti.pseudo)
    return pseudo in liste

def verification_Modif_MDP(mdp,mdpConfirmation):
    return mdp == mdpConfirmation

def modifier_MDP(mdp,pseudo):
    utilisateur = get_utilisateurInfo(pseudo)
    m = sha256()
    m.update(mdp.encode())
    passwd = m.hexdigest()
    utilisateur.password = passwd
    db.session.commit()


def verification_Modif_Profil(pseudoOriginal,pseudo):
    return (pseudo != pseudoOriginal and not pseudo_utilise(pseudo)) or pseudoOriginal == pseudo
    
def modifier_Profil(pseudoOriginal,pseudo,avatar,mail):
    utilisateur = get_utilisateurInfo(pseudoOriginal)
    utilisateur.pseudo = pseudo
    utilisateur.mail = mail
    utilisateur.avatar = avatar
    db.session.commit()
        


def ajouter_utilisateur_bd(pseudo,password):
    listeId =[]
    all_infoUser = getAllIdUser()
    for elem in all_infoUser:
        listeId.append(elem.id)
    id = getIdMax(listeId)+1
    m = sha256()
    m.update(password.encode())
    a = Utilisateur(id=id,avatar="https://image.flaticon.com/icons/png/512/94/94386.png", mail=" ", pseudo=pseudo, password=m.hexdigest())
    db.session.add(a)
    db.session.commit()

@login_manager.user_loader
def load_user(username):
    return Utilisateur.query.get(username)